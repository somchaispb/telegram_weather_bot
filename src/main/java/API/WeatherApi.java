package API;

import com.somchai.entities.Weather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApi {
    @GET("data/2.5/weather?lang=ru")
    Call<Weather> getWeatherByName(@Query("q") String city, @Query("APPID") String apiKey);

    @GET("data/2.5/weather?lang=ru")
    Call<Weather> getWeatherByLocation(@Query("lat") float lat,@Query("lon") float lon, @Query("APPID") String apiKey);
}
