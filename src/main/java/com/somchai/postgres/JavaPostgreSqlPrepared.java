package com.somchai.postgres;

import com.somchai.entities.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaPostgreSqlPrepared {

    private String url = "jdbc:postgresql://localhost:5432/weather";
    private String DBuser;
    private String password;


    public JavaPostgreSqlPrepared() {
        try (InputStream input = getClass().getResourceAsStream("/config.properties")) {
            Properties prop = new Properties();

            prop.load(input);

            DBuser = prop.getProperty("db.userId");
            password = prop.getProperty("db.password");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void insertQuery(User user) {

        String query = "INSERT INTO subscribe(timestamp, userId, issubscribed, chatId, longtitude, latitude, username, locationname) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection con = DriverManager.getConnection(url, DBuser, password);
             PreparedStatement pst = con.prepareStatement(query)) {

            pst.setTimestamp(1, user.getTimestamp());
            pst.setInt(2, user.getUserId());
            pst.setBoolean(3, user.isSubscribed());
            pst.setLong(4, user.getChatId());
            pst.setFloat(5, user.getLongtitude());
            pst.setFloat(6, user.getLatitude());
            pst.setString(7, user.getUsername());
            pst.setString(8, user.getLocationname());
            pst.executeUpdate();

        } catch (SQLException ex) {

            Logger lgr = Logger.getLogger(JavaPostgreSqlPrepared.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public User selectWhereQuery(int userId) {
        User user = new User();
        try (Connection con = DriverManager.getConnection(url, DBuser, password);
             PreparedStatement pst = con.prepareStatement("SELECT * FROM public.subscribe WHERE userid = userId");
             ResultSet rs = pst.executeQuery()) {

            while (rs.next()) {
                user.setTimestamp(rs.getTimestamp(2));
                user.setUserId(rs.getInt(3));
                user.setSubscribed(rs.getBoolean(4));
                user.setChatId(rs.getLong(5));
                user.setLongtitude(rs.getFloat(6));
                user.setLatitude(rs.getFloat(7));
                user.setUsername(rs.getString(8));
                user.setLocationname(rs.getString(9));
            }
        } catch (SQLException ex) {

            Logger lgr = Logger.getLogger(this.getClass().toString());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return user;
    }

    public int updateQuery(User user) {
        int affectedRows = 0;
        try (Connection con = DriverManager.getConnection(url, DBuser, password);
             PreparedStatement pst = con.prepareStatement("UPDATE public.subscribe set issubscribed = ? where userid = ?")) {
            pst.setBoolean(1, user.isSubscribed());
            pst.setInt(2, user.getUserId());
            affectedRows = pst.executeUpdate();
        } catch (SQLException ex) {

            Logger lgr = Logger.getLogger(this.getClass().toString());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return affectedRows;
    }

    public Collection<User> getSubscribedUsers() {
        List<User> users = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, DBuser, password);
             PreparedStatement pst = con.prepareStatement("SELECT * FROM public.subscribe where issubscribed = true");
             ResultSet rs = pst.executeQuery()) {

            while (rs.next()) {
                User user = new User();
                user.setTimestamp(rs.getTimestamp(2));
                user.setUserId(rs.getInt(3));
                user.setSubscribed(rs.getBoolean(4));
                user.setChatId(rs.getLong(5));
                user.setLongtitude(rs.getFloat(6));
                user.setLatitude(rs.getFloat(7));
                user.setUsername(rs.getString(8));
                user.setLocationname(rs.getString(9));
                users.add(user);
            }
        } catch (SQLException ex) {

            Logger lgr = Logger.getLogger(this.getClass().toString());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return users;
    }

}
