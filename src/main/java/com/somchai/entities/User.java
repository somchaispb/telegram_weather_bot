package com.somchai.entities;

import java.sql.Timestamp;

public class User {

    private Timestamp timestamp;
    private int userId;
    private boolean isSubscribed;
    private long chatId;
    private float longtitude;
    private  float latitude;
    private String username;
    private String locationname;

    public String getLocationname() {
        return locationname;
    }

    public void setLocationname(String locationname) {
        this.locationname = locationname;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public float getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(float longtitude) {
        this.longtitude = longtitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User{" +
                "timestamp=" + timestamp +
                ", userId=" + userId +
                ", isSubscribed=" + isSubscribed +
                ", chatId=" + chatId +
                ", longtitude=" + longtitude +
                ", latitude=" + latitude +
                ", username='" + username + '\'' +
                '}';
    }
}
