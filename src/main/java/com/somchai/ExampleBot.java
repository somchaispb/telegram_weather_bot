package com.somchai;

import com.somchai.entities.Main;
import com.somchai.entities.User;
import com.somchai.entities.Weather;
import com.somchai.entities.Weather_;
import com.somchai.postgres.JavaPostgreSqlPrepared;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ForceReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import schedule.DataBaseExecutor;
import schedule.MyTask;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.*;

public class ExampleBot extends TelegramLongPollingBot {

    private static final Logger LOG = LogManager.getLogger(ExampleBot.class);
    private static final String BOT_NAME = "weather_forecast_itmo_bot";
    private static final String ROUND_PUSHPIN = "\uD83D\uDCCD";
    private String BOT_TOKEN;
    private WeatherService weatherApi;
    private User user;

    public ExampleBot(DefaultBotOptions options) {
        super(options);
        weatherApi = new WeatherService();
        user = new User();
        DataBaseExecutor dataBaseExecutor = new DataBaseExecutor(new CheckSubscriptionsTask());
        try (InputStream input = getClass().getResourceAsStream("/config.properties")) {
            Properties prop = new Properties();
            prop.load(input);

            BOT_TOKEN = prop.getProperty("bot.token");

            dataBaseExecutor.startExecutionAt(9, 0, 0);


        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void onUpdateReceived(Update update) {
        update.getUpdateId();
        Message msg = update.getMessage();
        String response;
        if (update.hasMessage()) {
            if (msg.hasLocation() && msg.isReply()) {
                user.setLongtitude(msg.getLocation().getLongitude());
                user.setLatitude(msg.getLocation().getLatitude());
                sendWeatherForecast(msg);
                return;
            }
            if (msg.getText().equals("\uD83D\uDCCDLocation")) {
                sendPendingLocationResponse(msg);
                return;
            }
            if (msg.getText().equals("Ручной ввод")) {
                sendPendingManualResponse(msg);
                return;
            }
            if (msg.isReply() && msg.getReplyToMessage().getText().equals("Напишите интересующую локацию")) {
                sendWeatherForecastByName(msg);
                return;
            }
            if (msg.getText().equals("/subscribe")) {
                sendPendingLocationResponse(msg);
                return;
            }
            if (msg.isReply() && msg.getReplyToMessage().getText().equals("Жду вашей локации")) {
                Long chatId = msg.getChatId();
                int userId = msg.getFrom().getId();
                String username = msg.getFrom().getUserName();
                user.setUserId(userId);
                user.setTimestamp(new Timestamp(System.currentTimeMillis()));
                user.setChatId(chatId);
                user.setUsername(username);
                user.setLocationname(msg.getText());
                if (weatherApi.isSubscribed(userId)) {
                    response = "Вы уже подписаны на ежедневный прогноз";
                } else {
                    user.setSubscribed(true);
                    weatherApi.subscribe(user);
                    response = "Вы успешно подписались на ежедневный прогноз";
                }
                try {
                    execute(new SendMessage().setText(response)
                            .setChatId(chatId));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
            if (msg.getText().equals("/unsubscribe")) {
                Long chatId = msg.getChatId();
                int userId = msg.getFrom().getId();
                user.setUserId(userId);
                user.setSubscribed(false);
                weatherApi.unsubscribe(user);
                response = "Вы успешно отписались от ежедневного прогноза";
                try {
                    execute(new SendMessage().setText(response)
                            .setChatId(chatId));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
            else {
                sendHelloMessage(update, msg);
            }
        } else if (update.hasCallbackQuery()) {
            Long chatId = update.getCallbackQuery().getMessage().getChatId();
            try {
                switch (update.getCallbackQuery().getData()) {
                    case "current":
                        execute(setButtons(chatId, update.getCallbackQuery().getMessage().getMessageId()));
                        response = "Необходима локация";
                        break;
                    default:
                        response = "Неизвестная команда";
                        break;
                }
                execute(new SendMessage().setText(response)
                        .setChatId(chatId));
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

    }

    public void sendPendingLocationResponse(Message msg) {
        ForceReplyKeyboard forceReplyKeyboard = getForceReply();
        try {
            execute(new SendMessage().setReplyToMessageId(msg.getMessageId()).setReplyMarkup(forceReplyKeyboard).setText("Жду вашей локации").setChatId(msg.getChatId()));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void sendPendingManualResponse(Message msg) {
        ForceReplyKeyboard forceReplyKeyboard = getForceReply();
        try {
            execute(new SendMessage().setReplyToMessageId(msg.getMessageId()).setReplyMarkup(forceReplyKeyboard).setText("Напишите интересующую локацию").setChatId(msg.getChatId()));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void sendHelloMessage(Update update, Message msg) {
        String command = update.getMessage().getText();
        if (command.equals("/start")) {
            String helloMessage = "Привет " + update.getMessage().getFrom().getUserName() + " это тестовый погодный бот";
            try {
                sendMsg(msg, helloMessage);
                execute(sendInlineKeyBoardMessage(update.getMessage().getChatId()));
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendWeatherForecastByName(Message msg) {
        try {
            Weather weather = weatherApi.sendByCity(msg.getText());

            Main main = weather.getMain();

            double temp = main.getTemp() - 273;

            List<Weather_> weather_s = weather.getWeather();
            String clouds = "";
            for (Weather_ w : weather_s) {
                clouds = w.getDescription();
            }
            if (clouds.contains("облач")) {
                clouds += " \u2601";
            }
            String message = "В вашей локации " + weather.getName() + "\n" +
                    "Температура: " + (int) temp + " Градусов цельсия\n" +
                    clouds;
            sendMsg(msg, message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendWeatherForecast(Message msg) {
        float longitude;
        float latitude;
        try {
            longitude = msg.getLocation().getLongitude();
            latitude = msg.getLocation().getLatitude();
            Weather weather = weatherApi.sendByLocation(latitude, longitude);

            Main main = weather.getMain();

            double temp = main.getTemp() - 273;

            List<Weather_> weather_s = weather.getWeather();
            String clouds = "";
            for (Weather_ w : weather_s) {
                clouds = w.getDescription();
            }
            if (clouds.contains("облач")) {
                clouds += " \u2601";
            }
            String message = "В вашей локации " + weather.getName() + "\n" +
                    "Температура: " + (int) temp + " Градусов цельсия\n" +
                    clouds;
            sendMsg(msg, message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void sendMsg(Message msg, String text) {
        SendMessage s = new SendMessage();
        s.setChatId(msg.getChatId());
        s.setText(text);
        try {
            execute(s);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public static SendMessage sendInlineKeyBoardMessage(long chatId) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        InlineKeyboardButton inlineKeyboardButton1 = new InlineKeyboardButton();
        inlineKeyboardButton1.setText("Текущая погода");
        inlineKeyboardButton1.setCallbackData("current");
        List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();
        keyboardButtonsRow1.add(inlineKeyboardButton1);
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        rowList.add(keyboardButtonsRow1);
        inlineKeyboardMarkup.setKeyboard(rowList);
        return new SendMessage().setChatId(chatId).setText("Хотите узнать текущую погоду?").setReplyMarkup(inlineKeyboardMarkup);
    }

    public synchronized SendMessage setButtons(long chatId, int messageId) {
        // Создаем клавиуатуру
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
//        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        // Создаем список строк клавиатуры
        List<KeyboardRow> keyboard = new ArrayList<>();

        // Первая строчка клавиатуры
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        // Добавляем кнопки в первую строчку клавиатуры
        keyboardFirstRow.add(new KeyboardButton(ROUND_PUSHPIN + "Location"));

        // Вторая строчка клавиатуры
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        keyboardSecondRow.add(new KeyboardButton("Ручной ввод"));

        // Добавляем все строчки клавиатуры в список
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        replyKeyboardMarkup.setKeyboard(keyboard);
        return new SendMessage().setChatId(chatId).setReplyToMessageId(messageId).setText("Пришлите вашу локацию").setReplyMarkup(replyKeyboardMarkup);
    }


    private static ForceReplyKeyboard getForceReply() {
        ForceReplyKeyboard forceReplyKeyboard = new ForceReplyKeyboard();
        forceReplyKeyboard.setSelective(true);
        return forceReplyKeyboard;
    }

    public synchronized void answerCallbackQuery(String callbackId, String message) {
        AnswerCallbackQuery answer = new AnswerCallbackQuery();
        answer.setCallbackQueryId(callbackId);
        answer.setText(message);
//        answer.setShowAlert(true);
        try {
            execute(answer);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void sendHideKeyboard(Long chatId) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId.toString());
        sendMessage.enableMarkdown(true);
        sendMessage.setText("Спасибо!");

        ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();
        replyKeyboardRemove.setSelective(true);
        sendMessage.setReplyMarkup(replyKeyboardRemove);

        execute(sendMessage);
    }

    public class CheckSubscriptionsTask implements MyTask {
        @Override
        public void executeTask() {
            JavaPostgreSqlPrepared db = new JavaPostgreSqlPrepared();
            Collection<User> users = db.getSubscribedUsers();
            WeatherService weatherApi = new WeatherService();
            SendMessage sendMessage = new SendMessage();
            users.forEach(user -> {
                try {
                    Weather weather = user.getLocationname() != null ? weatherApi.sendByCity(user.getLocationname()) : weatherApi.sendByLocation(user.getLatitude(), user.getLongtitude());

                    Main main = weather.getMain();

                    double temp = main.getTemp() - 273;

                    List<Weather_> weather_s = weather.getWeather();
                    String clouds = "";
                    for (Weather_ w : weather_s) {
                        clouds = w.getDescription();
                    }

                    String message = "В вашей локации " + weather.getName() + "\n" +
                            "Температура: " + (int) temp + " Градусов цельсия\n" +
                            clouds;
                    sendMessage.enableMarkdown(true);
                    sendMessage.setChatId(user.getChatId());
                    sendMessage.setText(message);
                    execute(sendMessage);
                } catch (IOException | TelegramApiException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public String getBotUsername() {
        return BOT_NAME;
    }

    public String getBotToken() {
        return BOT_TOKEN;
    }
}
