package com.somchai;

import API.ApiClient;
import com.somchai.entities.User;
import com.somchai.entities.Weather;
import com.somchai.postgres.JavaPostgreSqlPrepared;
import retrofit2.Call;

import java.io.*;
import java.util.*;

public class WeatherService {

    private String API_KEY;
    private JavaPostgreSqlPrepared javaPostgreSqlPrepared;

    public WeatherService() {
        try (InputStream input = getClass().getResourceAsStream("/config.properties")) {

            Properties prop = new Properties();

            prop.load(input);
            API_KEY = prop.getProperty("api.key");
            javaPostgreSqlPrepared = new JavaPostgreSqlPrepared();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public Weather sendByCity(String city) throws IOException {
        API.WeatherApi weatherApi = ApiClient.getClient().create(API.WeatherApi.class);
        Call<Weather> call2 = weatherApi.getWeatherByName(city, API_KEY);

        Weather weather = call2.execute().body();

        return weather;
    }

    public Weather sendByLocation(float lat, float lon) throws IOException {
        API.WeatherApi weatherApi = ApiClient.getClient().create(API.WeatherApi.class);
        Call<Weather> call2 = weatherApi.getWeatherByLocation(lat, lon, API_KEY);

        Weather weather = call2.execute().body();

        return weather;
    }


    public void subscribe(User user) {
        javaPostgreSqlPrepared.insertQuery(user);
//        Collection<User> users = javaPostgreSqlPrepared.getSubscribedUsers();
//        System.out.println(users);
//        schedule.setSchedule(4000);
    }

    public void unsubscribe(User user) {
        javaPostgreSqlPrepared.updateQuery(user);
//        schedule.cancelSchedule();
    }

    public boolean isSubscribed(int userId) {
        User DBuser = javaPostgreSqlPrepared.selectWhereQuery(userId);
        return DBuser.isSubscribed();
    }

}
