package schedule;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;


public interface MyTask {
    void executeTask();
}
